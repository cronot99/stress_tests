#!/bin/bash

VAR=0

while true
do
	if [ $VAR -eq 0 ]
	then
		VAR=$(( $VAR + 1 ))
	else
		VAR=$(( $VAR - 1 ))
	fi
done
