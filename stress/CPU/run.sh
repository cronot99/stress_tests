#!/bin/bash

STRESS_APP=./stress_cpu
CPU_COUNT=$(cat /proc/cpuinfo | grep processor | wc -l)
COUNTER=1
KEY='none'

while getopts 'n:s:h' opt
do
	case $opt in
		n) CPU_COUNT=$OPTARG ;;
		s) STRESS_APP=$OPTARG ;;
		h) cat << EOF
Stress CPU application
-n: Number of cores to stress
-s: Stress application path
-h: Display this message
EOF
exit 0
;;
		\?) echo Operación no soportada
		exit 0
		;;
	esac
done


taskset -c 0 $STRESS_APP &
PIDS=$!

while [ $COUNTER -lt $CPU_COUNT ]
do
	taskset -c $COUNTER $STRESS_APP &
	PIDS="$PIDS $!"
	COUNTER=$(( $COUNTER + 1 ))
done

echo 'Press q to quit'
while [ $KEY != 'q' ]
do
	read -s -n 1 KEY
done

kill -9 $PIDS
