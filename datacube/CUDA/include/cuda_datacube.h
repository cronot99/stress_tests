extern "C" void dcMediansBandInt16(short *, short *, bool *, int, int, int, bool, int, short);
extern "C" void dcMediansBandInt32(int *, int *, bool *, int, int, int, bool, int, int);
extern "C" void dcMediansBandFloat64(double *, double *, bool *, int, int, int, bool, int, double);
